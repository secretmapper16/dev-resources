# Development Resources

## UP Center for Student Innovations

### Introduction

This repository serves as the compilation of every development **guides**, **tutorials**, **tips**, and **conventions** of
this organization.

### Contents

* [Development Tools](https://gitlab.com/up-csi/dev-resources/blob/master/development_tools.md)
* [Learn Ruby on Rails](https://gitlab.com/up-csi/dev-resources/blob/master/learn_ruby_on_rails.md)
* [Setting up MEAN](https://gitlab.com/up-csi/dev-resources/blob/master/setting_up_mean.md)
* [Installing pry for Rails 4](https://gitlab.com/up-csi/dev-resources/blob/master/pry_for_rails_linux.md)
* [Building your own LNPP Stack](https://gitlab.com/up-csi/dev-resources/blob/master/build_lnpp_stack.md)
* [Android Development Environment](https://gitlab.com/up-csi/dev-resources/blob/master/android_dev_env.md)
* [Learn Mezzanine](https://gitlab.com/up-csi/dev-resources/blob/master/learn_mezzanine.md)
