# CSI Development Resources

## Development Tools

### Note:
This guide was made and tested using debian-based operating systems such as **Ubuntu (Precise Pangolin)**,
**ElementaryOS (Luna)**, **Linux Mint (Cinnamon)**.

Before installing, please make sure to update and upgrade your system.
```bash
sudo apt-get update
sudo apt-get upgrade
```

### Installing Firefox

To install Firefox, input the following via terminal:
```bash
sudo apt-get install firefox
```

### Installing Sublime Text 3

To install Sublime Text 3, input the following via terminal:
```bash
sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo apt-get update
sudo apt-get install sublime-text-installer
```

Congrats! Now you have Sublime Text 3 installed in your machine.

Customize your editor for more uniform environment:

1. Install Sublime's package manager. Instructions can be found here: ```https://sublime.wbond.net/installation```
2. Open SublimeText 3, select ```Package Control:Install Package``` (shortcut: CTRL+SHIFT+P)
3. Search and install ```Theme - Soda SolarizedDark```
4. Open Preferences > Settings - User and activate the theme, color scheme, and indent using spaces with this configuration:

```javascript
{
  "tab_size": 2,
  "translate_tabs_to_spaces": true,
  "detect_indentation": false,
  "color_scheme": "Packages/Color Scheme - Default/Solarized (Dark).tmTheme",
  "theme": "Soda SolarizedDark.sublime-theme",
  "ignored_packages":
  [
    "Vintage"
  ]
}
```

Finally, restart Sublime Text.

### Installing Git

To install git, input the following via terminal:
```bash
sudo apt-get install git
```
