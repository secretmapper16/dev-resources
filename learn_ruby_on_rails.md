# CSI Development Resources

## Learn Ruby on Rails

### Introduction

This guide is for everyone who wants to learn [Ruby on Rails](http://www.rubyonrails.org). sqlite3 is the suggested database management system in this guide. Moreover, this will include our recommended environment and links for the most reliable and best tutorials on web.

### Set-up your environment

#### Dependencies

Install the following dependencies:

* git
* sqlite3
* nodeJs
* Rbenv
* Ruby
* Rails
* Bundle

**ElementaryOS/Ubuntu:**

Install **git**, **sqlite3**, **nodeJS**:
```bash
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install libssl-dev build-essential
sudo apt-get install sqlite3 libsqlite3-dev
sudo apt-get install git nodejs
```

Note: If this is your first time on git, it's advisable to configure your credentials first:
```bash
git config --global user.name "Neil Calabroso"
git config --global user.email "nmcalabroso@up.edu.ph"
```

Install [Rbenv](https://github.com/sstephenson/rbenv). Complete the installation instructions including the optional part:```ruby-build```.

Upon installing **Rbenv**, restart your terminal and install Ruby 2.1.2:
```bash
rbenv install 2.1.2
rbenv local 2.1.2
rbenv global 2.1.2
```

Install **Rails**:
```bash
gem install rails --verbose 
#--verbose command was used to see if there are errors during installation
```

Install Bundler:
```bash
gem install bundler #restart terminal after the installation
```

### Tutorials
* If you do not have any experience on ruby, we suggest to complete the challenge by [TryRuby](http://www.tryruby.org). 
* With the set-up above, you are now ready for your [first web app tutorial](http://guides.rubyonrails.org/getting_started.html). You can now start at *section 3.2*.
* Since the [Getting Started Tutorial](http://www.guides.rubyonrails.org/getting_started.html) is too high-level (with the use of scaffolding), it is very much suggested to read and finish the book [Ruby on Rails Tutorial](http://www.rubyonrailstutorial.org/book). This book gives a thorough explanation about the workflow of **Rails**, **Unit-testing**, **Git**, and **Deployment**. 

### Further Notes

For a high quality and readable codebase, please make these conventions as references:

1. Ruby Style Guide ```https://github.com/bbatsov/ruby-style-guide```
2. Rails Style Guide ```https://github.com/bbatsov/rails-style-guide```
